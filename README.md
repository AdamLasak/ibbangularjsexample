----------------------------------------
-- iBillboard entry test in AngularJS --
----------------------------------------

== HOW TO EXECUTE ==

Just double-click on index.html in /app directory. 

== FOR PROGRAMMERS ==

Basic *.css styles are in /css directory. 

The Controller is in js/employeeTableController.js file. On the top controller, there's
some initialize variables and under is object for employees. You can see how the
structure of object looks like. I used it only for testing.

At the bottom are date functions used in md-datepicker from Angular Material.

> App require correct Angular Material library include

> All data from table are saved in $scope.employees

> I used also functions from Angular Material for Alert or Confirm dialog

> TODO: verification is missing in table when user editing a row